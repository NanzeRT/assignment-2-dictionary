#!/usr/bin/env python3
import unittest
import subprocess


class TestLab2(unittest.TestCase):
    def run_app(self, inp):
        pipe = subprocess.PIPE
        proc = subprocess.Popen(
            ["./app"], text=True, shell=True, stdin=pipe, stdout=pipe, stderr=pipe
        )
        stdout, stderr = proc.communicate(input=inp)
        return (stdout.strip(), stderr.strip(), proc.returncode)

    def test_keyword(self):
        self.assertEqual(self.run_app("first"), ("first text", "", 0))
        self.assertEqual(
            self.run_app("second"), ("second text", "", 0)
        )

    def test_not_found(self):
        out = ("Not found", "", 1)
        self.assertEqual(self.run_app("a"), out)

    def test_too_long(self):
        out = ("Input error", "", 1)
        self.assertEqual(self.run_app("A" * 270), out)


if __name__ == "__main__":
    unittest.main()
