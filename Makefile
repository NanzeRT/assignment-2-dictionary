AS=nasm #Assembly compiler
ASFLAGS=-felf64 -g -isrc #Assembly flags
LD=ld #Linker
LDFLAGS= #Linker flags
SRCDIR=src#Source folder
BUILDDIR=build#Build folder
SOURCES=$(wildcard $(SRCDIR)/*.asm) #Sources
OBJECTS=$(SOURCES:$(SRCDIR)/%.asm=$(BUILDDIR)/%.o) #Object files
EXECUTABLE=app #Program name

all: $(SOURCES) $(EXECUTABLE)

$(BUILDDIR)/dict.o: $(SRCDIR)/lib.inc 
$(BUILDDIR)/main.o: $(SRCDIR)/dict.inc $(SRCDIR)/words.inc $(SRCDIR)/lib.inc $(SRCDIR)/colon.inc

#Create executable
$(EXECUTABLE): $(OBJECTS) 
	$(LD) $(LDFLAGS) $(OBJECTS) -o $@

#Compile assembly program
$(BUILDDIR)/%.o: $(SRCDIR)/%.asm
	@mkdir -p $(@D)
	$(AS) $(ASFLAGS) $< -o $@

run: $(EXECUTABLE)
	./$(EXECUTABLE)
 
#Clean folder
clean:
	rm -rf $(BUILDDIR) $(EXECUTABLE)

test: $(EXECUTABLE)
	./test.py

.PHONY: all clean run test
