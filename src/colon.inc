; colon scheme:
;   db [key] - null-terminated string
;   label:
;   dq next - pointer to next entry (0 if last)
;   dq key ptr - null-terminated string ptr
%macro colon 2
    %ifid %2
        %%key: db %1, 0
        %2:
            %ifdef last_dict_entry
                dq last_dict_entry
            %else
                dq 0
            %endif
            dq %%key

        %define last_dict_entry %2
    %else
        %error "colon: expected label"
    %endif
%endmacro

