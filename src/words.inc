%include "colon.inc"

section .rodata

colon "first", first
db "first text", 0

colon "second", second
db "second text", 0

colon "third", third
db "third text", 0

colon "very long name", very_long_name
db "very long name text", 0

word_list equ last_dict_entry
%undef last_dict_entry

