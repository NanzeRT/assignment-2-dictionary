%include "lib.inc"

section .text

global find_word

; finds entry in dictionary
; input:
;   rdi - pointer to key
;   rsi - pointer to dictionary
; output:
;   rax - pointer to entry
find_word:
    ; dict scheme:
    ;   dq next - pointer to next entry (0 if last)
    ;   dq key - null-terminated string ptr
    ;   db [value] - null-terminated string
    NEXT_OFFSET equ 0
    KEY_OFFSET equ 8
    VALUE_OFFSET equ 16
    
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi

    .loop:
        test r13, r13
        jz .not_found
        mov rdi, [r13 + KEY_OFFSET]
        mov rsi, r12
        call string_equals
        test rax, rax
        jnz .found
        mov r13, [r13 + NEXT_OFFSET]
        jmp .loop
    .found:
        mov rax, r13
        add rax, VALUE_OFFSET
        pop r13
        pop r12
        ret
    .not_found:
        xor rax, rax
        pop r13
        pop r12
        ret

