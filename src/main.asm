%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUF_SIZE 256

section .rodata
error_msg: db "Input error", 0
not_found_msg: db "Not found", 0

section .bss
buf: resb BUF_SIZE

section .text

global _start

_start:
    mov rdi, buf
    mov rsi, BUF_SIZE
    call read_line
    test rax, rax
    jz .read_error

    mov rdi, buf 
    mov rsi, word_list
    call find_word
    test rax, rax
    jz .not_found

    mov rdi, rax 
    call print_string
    call print_newline

    xor rdi, rdi
    jmp exit
.read_error:
    mov rdi, error_msg
    call print_string
    call print_newline
    mov rdi, 1
    jmp exit
.not_found:
    mov rdi, not_found_msg
    call print_string
    call print_newline
    mov rdi, 1
    jmp exit
    

